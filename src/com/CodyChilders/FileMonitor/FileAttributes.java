package com.CodyChilders.FileMonitor;

/**
 * Created by Cody on 4/20/2017.
 */
public class FileAttributes
{
  private int hashcode;
  private Long fileSizeInBytes;

  public int getHashcode() {
    return hashcode;
  }

  public void setHashcode(int hashcode) {
    this.hashcode = hashcode;
  }

  public Long getFileSizeInBytes() {
    return fileSizeInBytes;
  }

  public void setFileSizeInBytes(Long fileSizeInBytes) {
    this.fileSizeInBytes = fileSizeInBytes;
  }

}
