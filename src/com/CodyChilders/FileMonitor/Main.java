package com.CodyChilders.FileMonitor;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Main
{

    public static void main(String[] args)
    {
        boolean isFirstRun = true;
        boolean isDirectoryToWatchValid = false;
        File directoryToWatch = null;

        if(args != null && !StringUtils.isEmpty(args[0])) {

            directoryToWatch = new File(args[0]);

            isDirectoryToWatchValid = 
				directoryToWatch.exists() && directoryToWatch.isDirectory();
        }

        if(isDirectoryToWatchValid) {
            monitorDirectory(isFirstRun, directoryToWatch);
        } else {
            printInvalidFilePathError();
        }
    }

    private static void monitorDirectory(boolean isFirstRun, File directoryToWatch) {

        Map<String, FileAttributes> fileNameFilePairs = null;

        while (true) {

            File[] filesInWatchedDirectory = directoryToWatch.listFiles();

            if (filesInWatchedDirectory != null) {
                
				//We need to know the longestFileNameSize so that
				//the columnPadding will be set equally for all the columns 
				//in the output.
				final int longestFileNameSize = 
					getLongestFileNameSize(filesInWatchedDirectory);

                final int columnPadding = longestFileNameSize + 2;

                boolean isFirstRow = true;
                for (File currentFile : filesInWatchedDirectory) {
                    
					if (!isFirstRun) {
                        
                        FileAttributes fileAttributes = fileNameFilePairs
                            .get(currentFile.getName());

                        final boolean fileHasChanged =
                            fileAttributes == null
									|| (fileAttributes.getHashcode() != currentFile.hashCode()
									|| fileAttributes.getFileSizeInBytes() != currentFile
                                .length());

                        if (fileHasChanged) {
                            System.out.println();
                            printFileData(columnPadding, isFirstRow, currentFile);
                        }
                    } else {
                        printFileData(columnPadding, isFirstRow, currentFile);
                        isFirstRow = false;
                    }
                }

                isFirstRun = false;
                fileNameFilePairs = getFileNameFilePairs(filesInWatchedDirectory);

                waitForNextRound();
            }
        }
    }

    private static void printInvalidFilePathError() {
        System.out.println("Invalid file path");
    }

    private static Map<String, FileAttributes> getFileNameFilePairs(File[] filesInWatchedDirectory)
    {
        Map<String, FileAttributes> fileNameAttributePairs = new HashMap<>();

        for(File currentFile : filesInWatchedDirectory)
        {
            FileAttributes fileAttributes = new FileAttributes();
            fileAttributes.setHashcode(currentFile.hashCode());
            fileAttributes.setFileSizeInBytes(currentFile.length());
            fileNameAttributePairs.put(currentFile.getName(), fileAttributes);
        }

        return fileNameAttributePairs;
    }

    private static void waitForNextRound()
    {
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    private static void printFileData(int columnPadding, boolean isFirstRow, File currentFile)
    {
        final int FILE_SIZE_COLUMN_PADDING = 15;

        if(isFirstRow)
        {
            final String filenameHeader = getPaddedColumnText("FileName", columnPadding);
            System.out.print(filenameHeader);

            final String fileSizeHeader = getPaddedColumnText("FileSize", FILE_SIZE_COLUMN_PADDING);
            System.out.print(fileSizeHeader);

            final String timeHeader = getPaddedColumnText("Time", columnPadding);
            System.out.print(timeHeader);

            System.out.println();
        }

        final String fileNameText = getPaddedColumnText(currentFile.getName(), columnPadding);
        final String fileSizeText =
            getPaddedColumnText(String.valueOf(currentFile.length()), FILE_SIZE_COLUMN_PADDING);
        final String dateText = getPaddedColumnText(String.valueOf(new Date()), columnPadding);

        System.out.print(fileNameText);
        System.out.print(fileSizeText);
        System.out.print(dateText);

        System.out.println();
    }

    private static String getPaddedColumnText(String textToPad, int longestFileNameSize)
    {
        return StringUtils.rightPad(textToPad, longestFileNameSize);
    }

    private static int getLongestFileNameSize(File[] filesInWatchedDirectory)
    {
        int longestFileNameSize = 0;
        for(File currentFile : filesInWatchedDirectory)
        {
            final int currentFileNameLength = currentFile.getName().length();

            if(currentFileNameLength > longestFileNameSize){
                longestFileNameSize = currentFileNameLength;
            }
        }

        return longestFileNameSize;
    }
}
